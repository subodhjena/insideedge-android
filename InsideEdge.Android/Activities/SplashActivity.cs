using System.Threading;
using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Views;

namespace InsideEdge.Android.Activities
{
    [Activity(Label = "InsideEdge", MainLauncher = true, Icon = "@drawable/icon", NoHistory = true)]
    public class SplashActivity : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            RequestedOrientation = ScreenOrientation.Portrait;
            RequestWindowFeature(WindowFeatures.NoTitle);

            SetContentView(Resource.Layout.splash);

            new Thread(() =>
            {
                Thread.Sleep(2000);
                RunOnUiThread(() => StartActivity(typeof (LoginActivity)));
            }).Start();
        }
    }
}