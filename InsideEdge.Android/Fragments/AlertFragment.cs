using Android.App;
using Android.Graphics;
using Android.OS;
using Android.Views;

namespace InsideEdge.Android.Fragments
{
    public class AlertFragment : Fragment
    {
        public AlertFragment()
        {
            RetainInstance = true;
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View ignored = base.OnCreateView(inflater, container, savedInstanceState);
            View view = inflater.Inflate(Resource.Layout.alertLayout, null);
            var fragment1 = FragmentManager.FindFragmentById<AlertRowFragment>(Resource.Id.AlertRow1_fragment);
            var fragment2 = FragmentManager.FindFragmentById<AlertRowFragment>(Resource.Id.AlertRow2_fragment);
            var fragment3 = FragmentManager.FindFragmentById<AlertRowFragment>(Resource.Id.AlertRow3_fragment);
            View layout = fragment1.View.FindViewById(Resource.Id.alertRowLinearLayout);
            layout.SetBackgroundColor(Color.ParseColor("#ED7D35"));
            layout = fragment2.View.FindViewById(Resource.Id.alertRowLinearLayout);
            layout.SetBackgroundColor(Color.ParseColor("#F29863"));
            layout = fragment3.View.FindViewById(Resource.Id.alertRowLinearLayout);
            layout.SetBackgroundColor(Color.ParseColor("#F2A879"));
            return view;
        }
    }
}