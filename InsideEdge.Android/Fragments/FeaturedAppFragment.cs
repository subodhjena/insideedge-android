using System.Collections.Generic;
using Android.Content;
using Android.OS;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;
using InsideEdge.Android.Activities;
using InsideEdge.Android.Adapters;
using InsideEdge.Android.ViewModels;

namespace InsideEdge.Android.Fragments
{
    public class FeaturedAppFragment : Fragment
    {
        private List<ApplicationViewModel> _apps;

        public FeaturedAppFragment()
        {
            RetainInstance = true;
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View ignored = base.OnCreateView(inflater, container, savedInstanceState);
            View view = inflater.Inflate(Resource.Layout.featuredAppDashboard, null);

            var grid = view.FindViewById<GridView>(Resource.Id.grid);
            _apps = Util.GenerateApps();
            grid.Adapter = new DashboardAdapter(Activity, _apps);

            grid.DeferNotifyDataSetChanged();
            view.Invalidate();

            grid.ItemClick += GridOnItemClick;
            
            return view;
        }

        private void GridOnItemClick(object sender, AdapterView.ItemClickEventArgs itemClickEventArgs)
        {
            var intent = new Intent(Activity, typeof (ApplicationDetailActivity));
            intent.PutExtra("Id", itemClickEventArgs.Id.ToString());
            StartActivity(intent);
        }
    }
}