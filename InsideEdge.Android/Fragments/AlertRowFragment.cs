using Android.App;
using Android.OS;
using Android.Views;

namespace InsideEdge.Android.Fragments
{
    public class AlertRowFragment : Fragment
    {
        public AlertRowFragment()
        {
            RetainInstance = true;
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View ignored = base.OnCreateView(inflater, container, savedInstanceState);
            View view = inflater.Inflate(Resource.Layout.alertRow, null);
            return view;
        }
    }
}