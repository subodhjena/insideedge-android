using Android.OS;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;

namespace InsideEdge.Android.Fragments
{
    public class SettingsFragment : Fragment
    {
        public SettingsFragment()
        {
            RetainInstance = true;
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View ignored = base.OnCreateView(inflater, container, savedInstanceState);
            View view = inflater.Inflate(Resource.Layout.settings_fragment, null);
            return view;
        }

        public override void OnCreateOptionsMenu(IMenu menu, MenuInflater inflater)
        {
            inflater.Inflate(Resource.Menu.main_activity_actions, menu);
        }
    }
}