using System.Collections.Generic;
using InsideEdge.Android.ViewModels;

namespace InsideEdge.Android.Adapters
{
    public static class Util
    {
        public static List<ApplicationViewModel> GenerateApps()
        {
            return new List<ApplicationViewModel>
            {
                new ApplicationViewModel
                {
                    Id = 0,
                    AppName = "Calender",
                    Installed = true,
                    Rating = 1,
                    CategoryName = "yelp",
                    Platform = "Android,iOS",
                    ReleaseDate = "23/04/2014",
                    Version = 1,
                    LastModifiedDate = "23/04/2014",
                    NumberOfUsers = 400,
                    Img = Resource.Drawable.icon_calendar,
                },
                new ApplicationViewModel
                {
                    Id = 1,
                    Img = Resource.Drawable.icon_gmail,
                    Installed = true,
                    AppName = "Google",
                    Rating = 2,
                    CategoryName = "Educational",
                    Platform = "Android",
                    ReleaseDate = "24/04/2014",
                    Version = 2,
                    LastModifiedDate = "25/04/2014",
                    NumberOfUsers = 300
                },
                new ApplicationViewModel
                {
                    Id = 2,
                    Img = Resource.Drawable.icon_drive,
                    Installed = false,
                    AppName = "Drive",
                    Rating = 3,
                    CategoryName = "Utilities",
                    Platform = "Android,iOS",
                    ReleaseDate = "25/04/2014",
                    Version = 3,
                    LastModifiedDate = "26/04/2014",
                    NumberOfUsers = 200
                },
                new ApplicationViewModel
                {
                    Id = 3,
                    Img = Resource.Drawable.icon_hangout,
                    Installed = true,
                    AppName = "Hangout",
                    Rating = 4,
                    CategoryName = "Sample Category",
                    Platform = "Android",
                    ReleaseDate = "26/04/2014",
                    Version = 4,
                    LastModifiedDate = "27/04/2014",
                    NumberOfUsers = 100
                },
            };
        }

        public static List<CategoriesViewModel> GenerateCategories()
        {
            return new List<CategoriesViewModel>
            {
                new CategoriesViewModel
                {
                    Id = 1,
                    CategoryName = "Social",
                    CategoryIocn = @"https://cdn3.iconfinder.com/data/icons/leaf/128/RSS.png",
                    CategoryDesc = "Social Apps",
                    AppCountCategoryWise = 120
                },
                new CategoriesViewModel
                {
                    Id = 2,
                    CategoryName = "Games",
                    CategoryIocn = @"https://cdn3.iconfinder.com/data/icons/leaf/128/lastfm.png",
                    CategoryDesc = "Games",
                    AppCountCategoryWise = 122
                },
                new CategoriesViewModel
                {
                    Id = 3,
                    CategoryName = "Security",
                    CategoryIocn = @"https://cdn3.iconfinder.com/data/icons/leaf/128/googleplus.png",
                    CategoryDesc = "Security Apps",
                    AppCountCategoryWise = 124
                },
                new CategoriesViewModel
                {
                    Id = 4,
                    CategoryName = "Educational",
                    CategoryIocn = @"https://cdn3.iconfinder.com/data/icons/leaf/128/facebook.png",
                    CategoryDesc = "Educational Apps",
                    AppCountCategoryWise = 124
                },
            };
        }

        public static UserViewModel GenerateUser()
        {
            UserViewModel user = new UserViewModel();
            user.Id = 1;
            user.UserName = "Subodh Jena";
            user.InstalledApps = 10;
            user.TotalComments = 134;
            user.TotalRatings = 123;
            user.Device = "Android : 1234-5678-1023-1243";
            return user;
        }


    }
}