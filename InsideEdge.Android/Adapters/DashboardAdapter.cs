﻿using System.Collections.Generic;
using Android.App;
using Android.Views;
using Android.Widget;
using InsideEdge.Android.ViewModels;

namespace InsideEdge.Android.Adapters
{
    public class DashboardAdapter : BaseAdapter<ApplicationViewModel>
    {
        private readonly Activity context;
        private readonly List<ApplicationViewModel> items;

        public DashboardAdapter(Activity context, List<ApplicationViewModel> items)
        {
            this.context = context;
            this.items = items;
        }

        public override ApplicationViewModel this[int position]
        {
            get { return items[position]; }
        }

        public override int Count
        {
            get { return items.Count; }
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            ApplicationViewModel item1 = items[position];
            ApplicationViewModel item = items[position];
            View view = convertView; // re-use an existing view, if one is available
            if (view == null) // otherwise create a new one
            {
                view = context.LayoutInflater.Inflate(Resource.Layout.linearLayoutInside, null);
            }
            //view.FindViewById<TextView>(Resource.Id.PhotoLabel).Text = Resource.String.text_label1.ToString();
            view.FindViewById<TextView>(Resource.Id.PhotoLabel).Text = item1.AppName;
            //view.FindViewById<ImageView>(Resource.Id.image).SetImageResource(Resource.Drawable.icon_hangout); 
            view.FindViewById<ImageView>(Resource.Id.image).SetImageResource(item1.Img);
            return view;
        }
    }
}