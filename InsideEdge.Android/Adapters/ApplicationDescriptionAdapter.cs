using System.Collections.Generic;
using Android.App;
using Android.Views;
using Android.Widget;

namespace InsideEdge.Android.Adapters
{
    public class ApplicationDescriptionAdapter : BaseAdapter<string>
    {
        private readonly Activity context;
        private readonly List<string> items;

        public ApplicationDescriptionAdapter(Activity context, List<string> items)
        {
            this.context = context;
            this.items = items;
        }

        public override string this[int position]
        {
            get { return items[position]; }
        }

        public override int Count
        {
            get { return items.Count; }
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            string item = items[position];
            View view = convertView; // re-use an existing view, if one is available
            if (view == null) // otherwise create a new one
                view = context.LayoutInflater.Inflate(Resource.Layout.appDescription, null);
            view.FindViewById<TextView>(Resource.Id.textView2).Text = Resource.String.text_label1.ToString();
            view.FindViewById<ImageView>(Resource.Id.imageRelative).SetImageResource(Resource.Drawable.icon_drive);
            return view;
        }
    }
}