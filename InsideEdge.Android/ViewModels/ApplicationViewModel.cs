using System;

namespace InsideEdge.Android.ViewModels
{
    public class ApplicationViewModel
    {
        public int Id { get; set; }
        public bool Installed { get; set; }
        public String AppName { get; set; }
        public String AppIcon { get; set; }
        public int Rating { get; set; }
        public String CategoryName { get; set; }
        public String Platform { get; set; }
        public String ReleaseDate { get; set; }
        public int Version { get; set; }
        public String LastModifiedDate { get; set; }
        public int NumberOfUsers { get; set; }
        public int Img { get; set; }
    }
}