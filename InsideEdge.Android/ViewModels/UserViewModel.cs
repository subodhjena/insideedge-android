using System;
using System.Collections.Generic;

namespace InsideEdge.Android.ViewModels
{
    public class UserViewModel
    {
        public long Id { get; set; }
        public String UserName { get; set; }
        public int InstalledApps { get; set; }
        public int TotalComments { get; set; }
        public int TotalRatings { get; set; }
        public String Device { get; set; }
    }
}